1
00:00:01,114 --> 00:00:13,311
hello everyone and welcome to Portland

2
00:00:13,390 --> 00:00:16,790
[aplausse]

3
00:00:16,790 --> 00:00:21,650
on stage we have, you should know everybody up here already but

4
00:00:21,650 --> 00:00:24,610
Steve: Introduce yourself first
Patty: I dont want to introduce myself

5
00:00:24,610 --> 00:00:27,610
Steve: This is Patty

6
00:00:29,670 --> 00:00:30,820
[aplausse]

7
00:00:31,871 --> 00:00:36,440
Patty: we have Steve Langasek, Geral Turner and case cook

8
00:00:36,760 --> 00:00:41,440
They are the reason for everything

9
00:00:43,160 --> 00:00:44,671
that goes right

10
00:00:45,360 --> 00:00:49,913
anything that goes wrong, you can blame on ??

11
00:00:50,880 --> 00:00:53,952
Steve: Well, you can blame it on Steve,
I dont know if you can blame him

12
00:00:54,320 --> 00:00:55,480
particularly

13
00:00:56,000 --> 00:00:57,160
- Thats it
- thats it

14
00:00:57,870 --> 00:01:01,080
ok, so Patty had the honor to welcoming you all

15
00:01:01,600 --> 00:01:05,680
to portland
welcome to portland, welcomo to debconf14

16
00:01:06,240 --> 00:01:07,561
its so good to see you here

17
00:01:10,880 --> 00:01:12,880
hows it the weather

18
00:01:13,480 --> 00:01:15,721
you getting enough rain?

19
00:01:16,072 --> 00:01:19,160
did it met everybody's expectations?

20
00:01:19,680 --> 00:01:22,920
So, who arrieved yesterday and had the chance to...

21
00:01:23,560 --> 00:01:24,630
so people started to setteling in
aclimatizing... dealing with jetlag

22
00:01:24,960 --> 00:01:26,751
everybody's got some farmer's market this morning

23
00:01:27,080 --> 00:01:29,920
enjoying the coffee from the coffee cart

24
00:01:30,440 --> 00:01:32,001
and everything.. good!

25
00:01:32,431 --> 00:01:35,800
yeah, its really good to have everybody here and

26
00:01:36,240 --> 00:01:39,640
hopefully you are ready for some great sessions this week

27
00:01:39,800 --> 00:01:41,560
we've got quite the line up

28
00:01:41,720 --> 00:01:43,440
the schedule this year is different form whats been in the past

29
00:01:43,671 --> 00:01:44,640
where rather than having a debcamp beforehand and a debconf

30
00:01:45,160 --> 00:01:49,681
we casted 2 separated weeks

31
99:59:59,999 --> 99:59:59,999
we are doing something were kinda intertiwing the two

32
99:59:59,999 --> 99:59:59,999
and so we've got a schedule that allows for a full set of talks

33
99:59:59,999 --> 99:59:59,999
but also some dedicated hack time

34
99:59:59,999 --> 99:59:59,999
so, take advantage of that, do great stuff

35
99:59:59,999 --> 99:59:59,999
enjoy the talks, talk to people, socialize

36
99:59:59,999 --> 99:59:59,999
all the goodness. hopefully you will find the format

37
99:59:59,999 --> 99:59:59,999
to your liking

38
99:59:59,999 --> 99:59:59,999
and if you dont, we would like to hear that feedback as well

39
99:59:59,999 --> 99:59:59,999
but, enjoy the conference... what else can i say

40
99:59:59,999 --> 99:59:59,999
you stole my introduction, which i was going to introduce a little bit

41
99:59:59,999 --> 99:59:59,999
the local team here, so, joel turner

42
99:59:59,999 --> 99:59:59,999
all-around great guy, awesome community member here in portland

43
99:59:59,999 --> 99:59:59,999
where is our cheesemaster??, which is one of the reasons why you should see his face

44
99:59:59,999 --> 99:59:59,999
coming up we have the cheese and wine party

45
99:59:59,999 --> 99:59:59,999
the traditional social event on monday

46
99:59:59,999 --> 99:59:59,999
so if you dont want the cheese to melt

47
99:59:59,999 --> 99:59:59,999
on your way to the venue, you probably wanna get it to him

48
99:59:59,999 --> 99:59:59,999
before the evening of monday

49
99:59:59,999 --> 99:59:59,999
so there will be information going on about that

50
99:59:59,999 --> 99:59:59,999
but this is gerald, thank him for all his great work

51
99:59:59,999 --> 99:59:59,999
on this conference

52
99:59:59,999 --> 99:59:59,999
[applause]

53
99:59:59,999 --> 99:59:59,999
and case cook, whose the master mind behind

54
99:59:59,999 --> 99:59:59,999
behind getting us a network

55
99:59:59,999 --> 99:59:59,999
for working on this conference

56
99:59:59,999 --> 99:59:59,999
as you see, we are actually embedded

57
99:59:59,999 --> 99:59:59,999
on the portland state network

58
99:59:59,999 --> 99:59:59,999
so you are using stardard psu network infrastructure

59
99:59:59,999 --> 99:59:59,999
rather than debconf instrastructure this year

60
99:59:59,999 --> 99:59:59,999
hopefully thats holding up for everybody

61
99:59:59,999 --> 99:59:59,999
we also have, wired hacklabs and other stuff

62
99:59:59,999 --> 99:59:59,999
as well if its not working for you but..

63
99:59:59,999 --> 99:59:59,999
hes trying to pass the credit to mike tiger

64
99:59:59,999 --> 99:59:59,999
who i dont see on the audience

65
99:59:59,999 --> 99:59:59,999
mike, if you are here, stand up!

66
99:59:59,999 --> 99:59:59,999
mike!

67
99:59:59,999 --> 99:59:59,999
also part of the local team who is local and hes in seattle

68
99:59:59,999 --> 99:59:59,999
where the rain actually falls

69
99:59:59,999 --> 99:59:59,999
other members of the local team

70
99:59:59,999 --> 99:59:59,999
if you could just stand up now and show your faces

71
99:59:59,999 --> 99:59:59,999
pietro, tony,... yeah weve got

72
99:59:59,999 --> 99:59:59,999
very commited team that put this conference together

73
99:59:59,999 --> 99:59:59,999
and thank you all

74
99:59:59,999 --> 99:59:59,999
Patty: they should be standing here with us

75
99:59:59,999 --> 99:59:59,999
Steve: we dont need to punish everybody

76
99:59:59,999 --> 99:59:59,999
anyway, what else can i say

77
99:59:59,999 --> 99:59:59,999
i do have a list of announcements

78
99:59:59,999 --> 99:59:59,999
will the owner of a white cheeves... no... ah

79
99:59:59,999 --> 99:59:59,999
patty: nobody is going to get that

80
99:59:59,999 --> 99:59:59,999
Steve: I think they have cars on other countries too... that joke ??

81
99:59:59,999 --> 99:59:59,999
I dont know

82
99:59:59,999 --> 99:59:59,999
announcements, first of all if you are sitting on this room

83
99:59:59,999 --> 99:59:59,999
you are not of the off camera area

84
99:59:59,999 --> 99:59:59,999
so just so you know, the set up this year

85
99:59:59,999 --> 99:59:59,999
do the orientations of the rooms and what not

86
99:59:59,999 --> 99:59:59,999
in order to be able to see audience members

87
99:59:59,999 --> 99:59:59,999
speaking on camera as well as

88
99:59:59,999 --> 99:59:59,999
effectively the actual rooms

89
99:59:59,999 --> 99:59:59,999
are 100% you might be film

90
99:59:59,999 --> 99:59:59,999
so, if you are concern about being filmed

91
99:59:59,999 --> 99:59:59,999
you take advantage of the hacklabs

92
99:59:59,999 --> 99:59:59,999
we have live streams for all of the talks, so you

93
99:59:59,999 --> 99:59:59,999
be able to ?? remotely

94
99:59:59,999 --> 99:59:59,999
in theory if you are concern about being

95
99:59:59,999 --> 99:59:59,999
not captured on camera, you are not going to be

96
99:59:59,999 --> 99:59:59,999
standing up and asking questions

97
99:59:59,999 --> 99:59:59,999
hopefully, this works for everybody

98
99:59:59,999 --> 99:59:59,999
if there are any concerns about it, you can come

99
99:59:59,999 --> 99:59:59,999
talk to me about it and we can try to figure out

100
99:59:59,999 --> 99:59:59,999
what can work for you better if thats

101
99:59:59,999 --> 99:59:59,999
not a solution

102
99:59:59,999 --> 99:59:59,999
adhoc talks schedulings. so part of what is going on here

103
99:59:59,999 --> 99:59:59,999
is we do have, during our blocks of hacktime, we know

104
99:59:59,999 --> 99:59:59,999
hacking is not a solo activity necesarily

105
99:59:59,999 --> 99:59:59,999
so we want to give you the opportunity to schedule spaces

106
99:59:59,999 --> 99:59:59,999
where you can sit down and have small groups discussions

107
99:59:59,999 --> 99:59:59,999
and what not so all of the rooms will be availables for that

108
99:59:59,999 --> 99:59:59,999
during the periods of time which are blocked as hacktime

109
99:59:59,999 --> 99:59:59,999
on the schedule

110
99:59:59,999 --> 99:59:59,999
the scheduling for those rooms will be done on the wiki

111
99:59:59,999 --> 99:59:59,999
the pages will be open for scheduling 24 hours in advance

112
99:59:59,999 --> 99:59:59,999
in order to make sure it is in fact ad hoc and not people

113
99:59:59,999 --> 99:59:59,999
squating the rooms for those kind of things

114
99:59:59,999 --> 99:59:59,999
but its available, so if you think

115
99:59:59,999 --> 99:59:59,999
oh, geez, id be good to have a place where we can

116
99:59:59,999 --> 99:59:59,999
have a meeting about this thing, thats avaiable to you as well

117
99:59:59,999 --> 99:59:59,999
debconf-discuss, if you are not subscribed, it is recommended

118
99:59:59,999 --> 99:59:59,999
that youd be

119
99:59:59,999 --> 99:59:59,999
just in terms of ?? information during the conference

120
99:59:59,999 --> 99:59:59,999
we try to get major things send out to debconf-announce

121
99:59:59,999 --> 99:59:59,999
so times theres a little of delay until these things are get out

122
99:59:59,999 --> 99:59:59,999
so debconf-discuss also... you know, theres a lot of information

123
99:59:59,999 --> 99:59:59,999
going around that doesnt necesarily comes from the organizers

124
99:59:59,999 --> 99:59:59,999
as organizers, but its useful for people to know about

125
99:59:59,999 --> 99:59:59,999
so if you want to tap in whats going on

126
99:59:59,999 --> 99:59:59,999
debconf-discuss, please make sure you subscribe

127
99:59:59,999 --> 99:59:59,999
its a very useful resource during the conference

128
99:59:59,999 --> 99:59:59,999
to keep track of whats going on

129
99:59:59,999 --> 99:59:59,999
other stuff, you know, if theres anything

130
99:59:59,999 --> 99:59:59,999
that you are not sure about, the frontdesk

131
99:59:59,999 --> 99:59:59,999
which im not sure... any frontdesk people here in the audience?

132
99:59:59,999 --> 99:59:59,999
yes, right back there theres frontdesk people

133
99:59:59,999 --> 99:59:59,999
so get them a round of applause

134
99:59:59,999 --> 99:59:59,999
[applause]

135
99:59:59,999 --> 99:59:59,999
they do a tremendous work every year of

136
99:59:59,999 --> 99:59:59,999
making sure the conference runs smoothly and they are the only

137
99:59:59,999 --> 99:59:59,999
fixed point whthin the entire system where people can go to and

138
99:59:59,999 --> 99:59:59,999
coordinate, so if you are not sure where something is

139
99:59:59,999 --> 99:59:59,999
or someone is or whats going on

140
99:59:59,999 --> 99:59:59,999
the frontdesk is there, its just down the hall

141
99:59:59,999 --> 99:59:59,999
its labeled frontdesk, etc

142
99:59:59,999 --> 99:59:59,999
and its a nexus... the brain of the conference right there

143
99:59:59,999 --> 99:59:59,999
we have a couple of changes of the schedule which happens late

144
99:59:59,999 --> 99:59:59,999
that i want to make sure people knows about

145
99:59:59,999 --> 99:59:59,999
there was some confusion about available meal times

146
99:59:59,999 --> 99:59:59,999
which lead to a lead change to our dinner schedule

147
99:59:59,999 --> 99:59:59,999
as well as our evening talks schedule

148
99:59:59,999 --> 99:59:59,999
so rather than have the last talk of the day being from 5 to 5:45

149
99:59:59,999 --> 99:59:59,999
it turns out the dinners at university's cafeteria is going to be served

150
99:59:59,999 --> 99:59:59,999
from 5 to 5:30, so rather than having everybody

151
99:59:59,999 --> 99:59:59,999
rush to get served in a 45 minutes block after the last talk

152
99:59:59,999 --> 99:59:59,999
the last talk of the evening each day is going to be from 7 to 7:45

153
99:59:59,999 --> 99:59:59,999
and that takes effect starting today so we will have dinner at 5

154
99:59:59,999 --> 99:59:59,999
and an after dinner talk at 7

155
99:59:59,999 --> 99:59:59,999
you might have noticed as well if you are in sponsored food

156
99:59:59,999 --> 99:59:59,999
that there was a bit of a problem this morning with

157
99:59:59,999 --> 99:59:59,999
the branch opening times, my apologies, there was some

158
99:59:59,999 --> 99:59:59,999
miscommunication issues with the university about that

159
99:59:59,999 --> 99:59:59,999
we beliveve we have straighnen that out

160
99:59:59,999 --> 99:59:59,999
so going forward the meal times are documented on the wiki

161
99:59:59,999 --> 99:59:59,999
and on the schedule brunch starting, it should start at 11

162
99:59:59,999 --> 99:59:59,999
to 13:30 i believe is the correct time

163
99:59:59,999 --> 99:59:59,999
dont take my work for it. the wiki and the schedule knows better

164
99:59:59,999 --> 99:59:59,999
than i do

165
99:59:59,999 --> 99:59:59,999
theres also the coffee, i think i eluded to that early

166
99:59:59,999 --> 99:59:59,999
we organized coffee to make available to all atendees

167
99:59:59,999 --> 99:59:59,999
if you show your badge, the drip coffee and expresso

168
99:59:59,999 --> 99:59:59,999
and ?? tea wich ?? latte, which is only two blocks

169
99:59:59,999 --> 99:59:59,999
and im going to point the right way, two blocks that way

170
99:59:59,999 --> 99:59:59,999
its a nice little coffee card, if you have never been to portland before

171
99:59:59,999 --> 99:59:59,999
carts are a thing here... food carts, beverage carts

172
99:59:59,999 --> 99:59:59,999
its the way to go. so theres a coffee cart under the parking garage

173
99:59:59,999 --> 99:59:59,999
so its great coffee, who here had the coffee already?

174
99:59:59,999 --> 99:59:59,999
Patty: whos desapointed with it?

175
99:59:59,999 --> 99:59:59,999
Steve: oh, nobody is desapointed

176
99:59:59,999 --> 99:59:59,999
or nobody will admit to it in front of patty

177
99:59:59,999 --> 99:59:59,999
[laughs]

178
99:59:59,999 --> 99:59:59,999
so yeah, take advantage of that its

179
99:59:59,999 --> 99:59:59,999
sponsored by the conference, choice of drinks, etc,...

180
99:59:59,999 --> 99:59:59,999
and they give you a discount if you want somethin other than those

181
99:59:59,999 --> 99:59:59,999
basic drinks as well

182
99:59:59,999 --> 99:59:59,999
okay, i need to ask about volunteers about the cheese and wine party

183
99:59:59,999 --> 99:59:59,999
i just asked for volunteers for the cheese and wine party

184
99:59:59,999 --> 99:59:59,999
so, im asking for volunteers for the cheese and wine party

185
99:59:59,999 --> 99:59:59,999
we do need people to help with, i believe is the moving

186
99:59:59,999 --> 99:59:59,999
of things and set up, so if you are going to be available on tuesday

187
99:59:59,999 --> 99:59:59,999
monday, 6 pm, we want some volunteers for that

188
99:59:59,999 --> 99:59:59,999
and gerald is going to be the person

189
99:59:59,999 --> 99:59:59,999
to, with coordinate as well

190
99:59:59,999 --> 99:59:59,999
and i believe we are going to have some announcement

191
99:59:59,999 --> 99:59:59,999
mail with more details going out today/ tomorrowish

192
99:59:59,999 --> 99:59:59,999
to try to ?? the details on that

193
99:59:59,999 --> 99:59:59,999
last thing i have, we do have 2 hacklabs

194
99:59:59,999 --> 99:59:59,999
if you have only seen this floor of the building so far

195
99:59:59,999 --> 99:59:59,999
well, we have 2 hacklabs, and we have the balcony, and we have the park

196
99:59:59,999 --> 99:59:59,999
and all these other wonderful spaces that are available

197
99:59:59,999 --> 99:59:59,999
the psu campus where you can feel absolutely free to hack

198
99:59:59,999 --> 99:59:59,999
but if you have only seen this floor

199
99:59:59,999 --> 99:59:59,999
you should be aware, theres a second hacklab which is downstaris

200
99:59:59,999 --> 99:59:59,999
the room numbers 238 and if you go down the stairs basically is this way

201
99:59:59,999 --> 99:59:59,999
and its called the browsie launch ??

202
99:59:59,999 --> 99:59:59,999
you'll see that sign first

203
99:59:59,999 --> 99:59:59,999
but that is available to you

204
99:59:59,999 --> 99:59:59,999
specially since this hacklab overhere which is room 333

205
99:59:59,999 --> 99:59:59,999
theres some issues with the climate control in there

206
99:59:59,999 --> 99:59:59,999
we have shade missing in one corner so

207
99:59:59,999 --> 99:59:59,999
its getting a lot of sun in the afternoons

208
99:59:59,999 --> 99:59:59,999
and its supposed to be rather warm this week

209
99:59:59,999 --> 99:59:59,999
so you might find yourself getting rather warm

210
99:59:59,999 --> 99:59:59,999
be aware that air conditioning is working great in room 38

211
99:59:59,999 --> 99:59:59,999
as far as i can tell, so, thats available to you

212
99:59:59,999 --> 99:59:59,999
and it will only cost you a short walk down the stairs or a short elevator trip to get there

213
99:59:59,999 --> 99:59:59,999
theres been an announcement on debconf-announcement

214
99:59:59,999 --> 99:59:59,999
i think, the late night hacklabs

215
99:59:59,999 --> 99:59:59,999
this building closes at 10 pm everyday

216
99:59:59,999 --> 99:59:59,999
they are keeping it open for us passed normal hours

217
99:59:59,999 --> 99:59:59,999
so we are able to make use of it, it normally closes at 6pm

218
99:59:59,999 --> 99:59:59,999
on sunday, we are keeping it open until 10pm

219
99:59:59,999 --> 99:59:59,999
for the conference so you will have access to this space

220
99:59:59,999 --> 99:59:59,999
however after 10 pm everybody have to be out of this building

221
99:59:59,999 --> 99:59:59,999
which means obviosly, since hacking doesnt obey

222
99:59:59,999 --> 99:59:59,999
time zones or schedules, we need to make sure that we

223
99:59:59,999 --> 99:59:59,999
are providing for you and the solution we have in place is

224
99:59:59,999 --> 99:59:59,999
the broadway residence hall, which is where most of the people staying on campus

225
99:59:59,999 --> 99:59:59,999
are staying, that building on the second floor

226
99:59:59,999 --> 99:59:59,999
which you get to through the 6 avenue entry, instead of the jackson streen entry

227
99:59:59,999 --> 99:59:59,999
so not the residential entrance, but the other entrance

228
99:59:59,999 --> 99:59:59,999
where you checked in, that seconf floor is basically

229
99:59:59,999 --> 99:59:59,999
available for our use

230
99:59:59,999 --> 99:59:59,999
there are various lunge chairs, there actually classrooms that are available

231
99:59:59,999 --> 99:59:59,999
if you want a more sturdy hacking space

232
99:59:59,999 --> 99:59:59,999
feel free to use that in the evenings

233
99:59:59,999 --> 99:59:59,999
if you are not staying on campus, you will not be able to get into

234
99:59:59,999 --> 99:59:59,999
that building on your own after 8 pm, i trust that you are all

235
99:59:59,999 --> 99:59:59,999
capable of organazing a solution

236
99:59:59,999 --> 99:59:59,999
ive never known debian people had any dificulty organizing around anything

237
99:59:59,999 --> 99:59:59,999
so, body up with somebody that is staying in the dorms

238
99:59:59,999 --> 99:59:59,999
feel free to come by

239
99:59:59,999 --> 99:59:59,999
its just a matter that you will not be able to get to the building

240
99:59:59,999 --> 99:59:59,999
but you are welcome to come over there and make use of that space

241
99:59:59,999 --> 99:59:59,999
everybody that is coming to the conference is welcome to use that space

242
99:59:59,999 --> 99:59:59,999
after... well 24h

243
99:59:59,999 --> 99:59:59,999
and that is all i have, and i think

244
99:59:59,999 --> 99:59:59,999
i have steve mcentire ?? had some announcement

245
99:59:59,999 --> 99:59:59,999
anything else?

246
99:59:59,999 --> 99:59:59,999
once again, welcome to debconf and i handle over to uncle steve

247
99:59:59,999 --> 99:59:59,999
oh, this thign is on, wow

248
99:59:59,999 --> 99:59:59,999
so a lot of people probably have noticed we had quite

249
99:59:59,999 --> 99:59:59,999
heated discussions in debian in the last year or so

250
99:59:59,999 --> 99:59:59,999
?? plenty who hasnt

251
99:59:59,999 --> 99:59:59,999
theres been a lot of disccusion going on, theres some people

252
99:59:59,999 --> 99:59:59,999
have got very very tired of it, theres have been some strong words

253
99:59:59,999 --> 99:59:59,999
in the middle of all however, there has been at least one person

254
99:59:59,999 --> 99:59:59,999
who seems to be able to keep his head, has not really

255
99:59:59,999 --> 99:59:59,999
well, lost its cool at all that we can see

256
99:59:59,999 --> 99:59:59,999
i know from talking to him in another venues as well

257
99:59:59,999 --> 99:59:59,999
hes being going through a massive amount of stress at work

258
99:59:59,999 --> 99:59:59,999
he just changed employers recently and everything

259
99:59:59,999 --> 99:59:59,999
well, that hasnt spill over at all in terms on hows hes dealing with other people

260
99:59:59,999 --> 99:59:59,999
in debian

261
99:59:59,999 --> 99:59:59,999
and i know i have been very impressed, a lot of us have

262
99:59:59,999 --> 99:59:59,999
and we have a little something to show appreciation

263
99:59:59,999 --> 99:59:59,999
russ, are you here?

264
99:59:59,999 --> 99:59:59,999
[applauses] please come up

265
99:59:59,999 --> 99:59:59,999
so this is very much instigated from some of the folks in the uk

266
99:59:59,999 --> 99:59:59,999
and in particular ?? cant be here this week so

267
99:59:59,999 --> 99:59:59,999
he trusted this to me

268
99:59:59,999 --> 99:59:59,999
??, my good friend from the uk as well

269
99:59:59,999 --> 99:59:59,999
was responsible for doing some of the manufacturing of this

270
99:59:59,999 --> 99:59:59,999
but russ, in conmemoration of your massive patience

271
99:59:59,999 --> 99:59:59,999
we would like to present you with the debcon

272
99:59:59,999 --> 99:59:59,999
ill let him read it outloud

273
99:59:59,999 --> 99:59:59,999
russ: oh, this is great, because now i dont have to decide what to say

274
99:59:59,999 --> 99:59:59,999
debcon5, discussions happens threads are short, problems get solved

275
99:59:59,999 --> 99:59:59,999
threads get long, arguments are repeated

276
99:59:59,999 --> 99:59:59,999
debconf4, links to threads get posted on reddit and HN,

277
99:59:59,999 --> 99:59:59,999
LWN writes articles

278
99:59:59,999 --> 99:59:59,999
debcon3, trolls show up, listmasters ban people

279
99:59:59,999 --> 99:59:59,999
debcon2, UN security committee meets

280
99:59:59,999 --> 99:59:59,999
GRs are proposed and voted on

281
99:59:59,999 --> 99:59:59,999
debcon1, laser cannons are launched into planetary orbit

282
99:59:59,999 --> 99:59:59,999
russ gets midly annoyed

283
99:59:59,999 --> 99:59:59,999
[applause]

284
99:59:59,999 --> 99:59:59,999
Steve: i think thats it for us

285
99:59:59,999 --> 99:59:59,999
in preparing the welcome talk i was a bit confused

286
99:59:59,999 --> 99:59:59,999
because i didnt know what to say.. and when consulting with people

287
99:59:59,999 --> 99:59:59,999
turns out this it is really it

288
99:59:59,999 --> 99:59:59,999
which is how i manged to actually miss the welcome talk

289
99:59:59,999 --> 99:59:59,999
for the last 3 debconfs running

290
99:59:59,999 --> 99:59:59,999
because is short, sweet and out of the way

291
99:59:59,999 --> 99:59:59,999
so... welcome and have a good conference

292
99:59:59,999 --> 99:59:59,999
[applause]
