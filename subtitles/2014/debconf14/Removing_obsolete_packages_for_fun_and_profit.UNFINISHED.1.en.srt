1
99:59:59,999 --> 99:59:59,999
Hi, everybody, I'm Eric Doland.

2
99:59:59,999 --> 99:59:59,999


3
99:59:59,999 --> 99:59:59,999
I'm the automake maintainer,
among other things.

4
99:59:59,999 --> 99:59:59,999
Today I'm going to talk about removing obsolete packages.

5
99:59:59,999 --> 99:59:59,999
This is meant to be a BOF.

6
99:59:59,999 --> 99:59:59,999
I only have sixteen slides,

7
99:59:59,999 --> 99:59:59,999
so I'm going to talk about my experiences

8
99:59:59,999 --> 99:59:59,999
in removing old versions of automake.

9
99:59:59,999 --> 99:59:59,999
Please feel free to interrupt with questions

10
99:59:59,999 --> 99:59:59,999
or criticisms or comments as we go.

11
99:59:59,999 --> 99:59:59,999
Hopefully with a bit of discussion about how to do this better in the future,

12
99:59:59,999 --> 99:59:59,999
or maybe not better, we'll see.

13
99:59:59,999 --> 99:59:59,999
So just to set the context a little bit,

14
99:59:59,999 --> 99:59:59,999
automake in Debian,

15
99:59:59,999 --> 99:59:59,999
I started maintaining it a really long time ago.

16
99:59:59,999 --> 99:59:59,999
It's making me feel old. 2002.

17
99:59:59,999 --> 99:59:59,999
This was around the time that automake 1.6 came out, I think.

18
99:59:59,999 --> 99:59:59,999
One of the problems automake has is

19
99:59:59,999 --> 99:59:59,999
that new releases of it usually have incompatibilities

20
99:59:59,999 --> 99:59:59,999
with backwards-compatible completely.

21
99:59:59,999 --> 99:59:59,999
There's various reasons why this is,

22
99:59:59,999 --> 99:59:59,999
it provides a very weird interface,

23
99:59:59,999 --> 99:59:59,999
because you're basically embedding things inside make files,

24
99:59:59,999 --> 99:59:59,999
and it's hard to control the API of a make file.

25
99:59:59,999 --> 99:59:59,999
Back in the day, there was, for a very long time,

26
99:59:59,999 --> 99:59:59,999
automake 1.4, and that just sort of worked,

27
99:59:59,999 --> 99:59:59,999
and then, automake 1.5 was introduced,

28
99:59:59,999 --> 99:59:59,999
Debian upgraded to automake 1.5, and lots of packages broke.

29
99:59:59,999 --> 99:59:59,999
So what ended up happening is that automake 1.5 got moved into its own package,

30
99:59:59,999 --> 99:59:59,999
and automake 1.4 was automake package.

31
99:59:59,999 --> 99:59:59,999
Nowadays, the older versions get moved into their own packages,

32
99:59:59,999 --> 99:59:59,999
and generally, the currently released version--the most up-to-date version--

33
99:59:59,999 --> 99:59:59,999
is the automake package.

34
99:59:59,999 --> 99:59:59,999
This has some downsides, because it's not necessarily fully backwards-compatible,

35
99:59:59,999 --> 99:59:59,999
but this is usually what people want.

36
99:59:59,999 --> 99:59:59,999
They want the latest version of automake, and they don't want to have to install

37
99:59:59,999 --> 99:59:59,999
a new package any time a new version of automake comes out.

38
99:59:59,999 --> 99:59:59,999
All the automake packages provide an alternative

39
99:59:59,999 --> 99:59:59,999
so that you can have your /usr/bin/automake

40
99:59:59,999 --> 99:59:59,999
be exactly which automake you want it to be.

41
99:59:59,999 --> 99:59:59,999
The priority of that alternative usually means that the latest version of automake

42
99:59:59,999 --> 99:59:59,999
is the one automatically selected, but of course, you can override that.

43
99:59:59,999 --> 99:59:59,999
There's a note here that, depending on automake directly, is a little risky,

44
99:59:59,999 --> 99:59:59,999
because fancy automake files, new versions of automake, might break you, potentially.

45
99:59:59,999 --> 99:59:59,999
Anyway, this is not the automake BOF, this is just a little context

46
99:59:59,999 --> 99:59:59,999
about why automake is what it is in Debian.

47
99:59:59,999 --> 99:59:59,999
So there's problems with this, because these backwards-compatible issues, as I said,

48
99:59:59,999 --> 99:59:59,999
were packaging all the versions of automake.

49
99:59:59,999 --> 99:59:59,999
Each new version was getting a new package, basically,

50
99:59:59,999 --> 99:59:59,999
and we've gone through a lot of automake packages over the years.

51
99:59:59,999 --> 99:59:59,999
I didn't count them up, but it looks like almost close to ten, I guess,

52
99:59:59,999 --> 99:59:59,999
which is a lot of packages you have to deal with.

53
99:59:59,999 --> 99:59:59,999
People will depend on these individual packages, for whatever reason,

54
99:59:59,999 --> 99:59:59,999
and then getting them out of the system, when they become old, is troublesome.

55
99:59:59,999 --> 99:59:59,999
As of the wheezy release, we had four different automakes in the wheezy release.

56
99:59:59,999 --> 99:59:59,999
So we had what the old, old--as you can see, 1.4, 1.9, 1.10, and 1.11--

57
99:59:59,999 --> 99:59:59,999
and they say that at one point we're going to have five different versions of automake,

58
99:59:59,999 --> 99:59:59,999
which is a lot of versions of automake.

59
99:59:59,999 --> 99:59:59,999
You can also see that the 1.4 version was released in 2002,

60
99:59:59,999 --> 99:59:59,999
when I started maintaining this stuff, so it's really old.

61
99:59:59,999 --> 99:59:59,999
No one should be using it, and no one should have been using it for the last ten years,

62
99:59:59,999 --> 99:59:59,999
but we sort of kept it around because people were like

63
99:59:59,999 --> 99:59:59,999
"oh, maybe there's old software that, you know, still depends on automake 1.4."

64
99:59:59,999 --> 99:59:59,999
Um, yeah.

65
99:59:59,999 --> 99:59:59,999
This is crazy. 
There are too many versions of automake.

66
99:59:59,999 --> 99:59:59,999
No one really wants them. So, I started out on this mission

67
99:59:59,999 --> 99:59:59,999
to bring us down to, hopefully, one or two versions of automake for the next release.

68
99:59:59,999 --> 99:59:59,999
I've been doing this over the last year, and I was makign these slides

69
99:59:59,999 --> 99:59:59,999
and I was thinking about, it sort of had these weird parallels

70
99:59:59,999 --> 99:59:59,999
with some things I'd read about, so now I present to you

71
99:59:59,999 --> 99:59:59,999
the five stages of removing an obsolete package from Debian.

72
99:59:59,999 --> 99:59:59,999
The first stage is denial.

73
99:59:59,999 --> 99:59:59,999
[laughter]

74
99:59:59,999 --> 99:59:59,999
"If I send mail to debian-devel asking everyone to very nicely

75
99:59:59,999 --> 99:59:59,999
move off of these old versions of automake, they'll just do it, right?

76
99:59:59,999 --> 99:59:59,999
I mean, doesn't everyone agree that that's what will happen?

77
99:59:59,999 --> 99:59:59,999
Yeah, so that's what I started out by doing.

78
99:59:59,999 --> 99:59:59,999
I'm going to talk a little bit about the tools and stuff that I used in this process,

79
99:59:59,999 --> 99:59:59,999
so this is sort of technical, and sort of procedural.

80
99:59:59,999 --> 99:59:59,999
If you've got questions about either side, just shoot up your hand.

81
99:59:59,999 --> 99:59:59,999
The first thing you do, or the first thing I did, is that I used grep-dctrl to figure out

82
99:59:59,999 --> 99:59:59,999
which packages were build dependent on automake.

83
99:59:59,999 --> 99:59:59,999
Automake basically has no actual binary dependencies, it's all build dependencies.

84
99:59:59,999 --> 99:59:59,999
There were one hundred and sixty-nine packages--source packages.

85
99:59:59,999 --> 99:59:59,999
That's a lot, but in Debian scale, it's not that much. It should be fine, right?

86
99:59:59,999 --> 99:59:59,999
Then, I used the dlist tool to turn that list of packages into a list of maintainers,

87
99:59:59,999 --> 99:59:59,999
with their packages. It's a very nice tool for just ??

88
99:59:59,999 --> 99:59:59,999
Then, I sent out an email debian-devel saying,

89
99:59:59,999 --> 99:59:59,999
"Here's my plan: I want to get rid of these old versions of automake.

90
99:59:59,999 --> 99:59:59,999
Here are the packages that are build-dependent on them.

91
99:59:59,999 --> 99:59:59,999
Please, do your part and fix this.

92
99:59:59,999 --> 99:59:59,999
I sent that mail out on May 27, 2013, so before last DebConf, which I was not at.

93
99:59:59,999 --> 99:59:59,999
Stage Two: Anger.

94
99:59:59,999 --> 99:59:59,999
People aren't fixing these things!

95
99:59:59,999 --> 99:59:59,999
I'm going to have to actually do something other than send an email. All right, fine.

96
99:59:59,999 --> 99:59:59,999
The next thing I did, to try to encourage people again to make this move,

97
99:59:59,999 --> 99:59:59,999
Lintian has this really nice facility. One of tests is actually--there's a list of obsolete packages.

98
99:59:59,999 --> 99:59:59,999
If you put your package in that list,

99
99:59:59,999 --> 99:59:59,999
lintian will complain any time anyone depends on that package,

100
99:59:59,999 --> 99:59:59,999
to try to force maintainers that are paying attention,

101
99:59:59,999 --> 99:59:59,999
that they should't actually be depending on these packages.

102
99:59:59,999 --> 99:59:59,999
That's good. It's cheap to do. Just sent the patch to lintian.

103
99:59:59,999 --> 99:59:59,999
I don't see any reason they wouldn't take it if it made sense.

104
99:59:59,999 --> 99:59:59,999
Now, I have to file bugs. People don't read debian-devel.

105
99:59:59,999 --> 99:59:59,999
It's sort of, it's fair I guess. So I basically just went through the list of packages I had,

106
99:59:59,999 --> 99:59:59,999
with a simple substitution script, so now it mailed a mail that was like

107
99:59:59,999 --> 99:59:59,999
"Please stop depending on automake blank." with a very boiler-plate thing saying

108
99:59:59,999 --> 99:59:59,999
"this is why we're getting rid of these old versions of automake"

109
99:59:59,999 --> 99:59:59,999
and I sent that off to BTS. Which is really easy, right? Because BTS is just email.

110
99:59:59,999 --> 99:59:59,999
So you can just do that. You can file a bunch of bugs that way.

111
99:59:59,999 --> 99:59:59,999
I should note that my initial email was a proposal for this mass bug filing, as well,

112
99:59:59,999 --> 99:59:59,999
which you're supposed to do as part of the procedure of sending out mass bug filings.

113
99:59:59,999 --> 99:59:59,999
When I did this, I had to file one hundred and seventeen bugs.

114
99:59:59,999 --> 99:59:59,999
There's fifty two packages that got fixed between me sending my initial email

115
99:59:59,999 --> 99:59:59,999
and the bugs. So that's good, I shouldn't be that angry, maybe.

116
99:59:59,999 --> 99:59:59,999
I did wait four months between these two events, though, so there's plenty of time.

117
99:59:59,999 --> 99:59:59,999
The other thing I used was using BTS user tags, to track all of these bugs very easily easily

118
99:59:59,999 --> 99:59:59,999
using this automake clean-up 2013 tag. I added the date because

119
99:59:59,999 --> 99:59:59,999
I knew this would not be the last automake clean-up that I would have to do,

120
99:59:59,999 --> 99:59:59,999
so I keep these separate.

121
99:59:59,999 --> 99:59:59,999
Always good to date your work if you think you might have to repeat it.

122
99:59:59,999 --> 99:59:59,999
Yeah, no, I did not finish in 2013. I started in 2013.

123
99:59:59,999 --> 99:59:59,999
It should've been automake clean-up 2015 or something, if I wanted the finish date.

124
99:59:59,999 --> 99:59:59,999
All right, so stage three. Bargaining.

125
99:59:59,999 --> 99:59:59,999
Okay, so, I'm past the anger stage, and now I'm like "okay, I've filed all these bugs,

126
99:59:59,999 --> 99:59:59,999
but not much is happening, so let me just fix this for you.

127
99:59:59,999 --> 99:59:59,999
I'll give you the exact fix on how to much to the newer version of automake.

128
99:59:59,999 --> 99:59:59,999
Here's the patch. Just apply it and upload your package.

129
99:59:59,999 --> 99:59:59,999
I've done all the work; I've done all the hard stuff."

130
99:59:59,999 --> 99:59:59,999
So, before I even did this, thirty four packages were fixed

131
99:59:59,999 --> 99:59:59,999
without supplying a patch. I began the patching in late October of 2013

132
99:59:59,999 --> 99:59:59,999
and--this is the hard part, this is the part that's tough to automate--in many cases,

133
99:59:59,999 --> 99:59:59,999
just switching to the new version of automake worked, and then it was easy,

134
99:59:59,999 --> 99:59:59,999
but that was maybe 50% of the cases.

135
99:59:59,999 --> 99:59:59,999
I didn't keep good statistics about how tough this was exactly,

136
99:59:59,999 --> 99:59:59,999
but in a lot of cases, I had to fiddle with the build system,

137
99:59:59,999 --> 99:59:59,999
or a lot of build rules would run automake itself,

138
99:59:59,999 --> 99:59:59,999
instead of using dh-autoreconf.

139
99:59:59,999 --> 99:59:59,999
There was a lot of actual fiddling with packages, trying to get them to build,

140
99:59:59,999 --> 99:59:59,999
and then just waiting to build these things, which can take a very long time,

141
99:59:59,999 --> 99:59:59,999
to test that this worked. So this is a lot of actual, tough work.

142
99:59:59,999 --> 99:59:59,999
What else? I used pbuilder to do the builds and then if I successfully built something,

143
99:59:59,999 --> 99:59:59,999
with the new version of automake,
I would just mail off a patch

144
99:59:59,999 --> 99:59:59,999
to the existing bug, in BTS, and flip the patch tag, saying "here's the patch."

145
99:59:59,999 --> 99:59:59,999
So I thought, okay, at this point, you know, there's patches out there

146
99:59:59,999 --> 99:59:59,999
for almost all of these problems.

147
99:59:59,999 --> 99:59:59,999
There should be a wave of uploads and everything should be great.

148
99:59:59,999 --> 99:59:59,999
Stage Four: Depression.

149
99:59:59,999 --> 99:59:59,999
That's not what happened. I was pretty much on my own.

150
99:59:59,999 --> 99:59:59,999
The responsible maintainers have already fixed this problem.

151
99:59:59,999 --> 99:59:59,999
So we're into this, sort of, long tale of people who don't care,

152
99:59:59,999 --> 99:59:59,999
or people who are too busy to deal with their packages or do anything here.

153
99:59:59,999 --> 99:59:59,999
Now I have to upload NMUs. I started this in late January, I think

154
99:59:59,999 --> 99:59:59,999

