#!/bin/bash
# Sync subtitles from debconfsubs git to meetings-archive
#
# This script was desgined to be run in meetings-archive server
# to update the subtitles folder publicly available on meetings-archive
#
# GOTCHA: this script only sync deleting on dest folders content (ie. .srt)
# If events/years folders change names, this wont touch them


#CONSTS
DEST='pub/debian-meetings'
#DRY_RUN='-n'

#MAIN
set -e
dir='debconfsubs/subtitles'
if [ ! -d "$dir" ]; then
	git clone https://anonscm.debian.org/git/debconfsubs/debconfsubs.git
fi

cd $dir
git pull

for year in */; do
	for yevent in "$year"*/; do

		origin=$yevent
		destination="../../$DEST/$yevent"subtitles

		echo
		echo "NEW COPY:"
		echo "pwd: $(pwd)"
		echo "ORIGIN: $origin"
		echo "DESTINATION: $destination"
		if [ ! -d $destination ]; then
			mkdir -p $destination
		fi

		rsync -avh $DRY_RUN --delete $origin $destination
	done
done
