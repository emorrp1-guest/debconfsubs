#!/usr/bin/env python3
# Scraps url and creates JSON_CACHE file
# This json is raw. It should be manually twisted to be uniform
# It doesnt overwrite already existing files

import subprocess
import re, os, json
from urllib.parse import urlparse
from os.path import basename, splitext
from pprint import pprint

SCRAP_CACHE_FILE = '.video.debian.net.scrap'
JSON_CACHE = '.video.debian.net.json'

url='http://meetings-archive.debian.net/pub/debian-meetings/'
cmd = ["wget", "-r", "-nd", "-np", "--spider", url]

videore = re.compile('.*\[video.*')
urlre = re.compile('.*  (http.*)')

class autodict(dict):
    def __missing__(self, key):
        value = self[key] = type(self)()
        return value

def update_cache():
    print("UPDATING CACHE")
    cmdresult = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, bufsize=1, universal_newlines=True)
    with open(SCRAP_CACHE_FILE, "w") as f:
        f.write(cmdresult.stdout)

if not os.path.isfile(SCRAP_CACHE_FILE):
    update_cache()

videos = autodict()

#this file needs to have the line with the url seized before the mime information
#or it will implode
with open(SCRAP_CACHE_FILE) as f:
    last_path_seen = ''
    vname = ''
    vdir = ''
    year = ''
    for line in f:
        m = videore.match(line)
        if m:
            vname = splitext(basename(last_path_seen))[0]
            #the dirs without /pub/debian-meetings/
            vdir = last_path_seen.split('/')[3:-1]
            year = vdir.pop(0)
            event = vdir.pop(0)

            videos[year][event][vname][os.path.join('/',*vdir)] = last_path_seen

            #print(vname + " " + str(vdir))
            next

        m = urlre.match(line)
        if m:
            #the whole path with the filename
            last_path_seen = urlparse(m.group(1)).path

if os.path.isfile(JSON_CACHE):
    pprint(videos)
    print()
    print(JSON_CACHE + ' alredy exists. Not Overwriting it')
else:
    print('No ' + JSON_CACHE + ' found. Saving it')
    with open(JSON_CACHE, "w") as f:
        json.dump(videos, f, sort_keys=True, indent=4)
